<?php get_header(); ?>
<?php
$ClubId = $_GET['club'];
$protocol = is_SSL() ? 'https://' : 'http://';
$request = wp_remote_get($api . "clubs/" . $ClubId);
// $request = wp_remote_get( $api . "clubs/6");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
$countathletes = count($data->rows->activeAthletes);
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php _e('Alle Boxclubs Content', 'swissboxing'); ?></p>
    </div>
    <div class="newsContent">
        <div class="col-lg-12 margin-bottom-3rem">
            <div class="col-md-6">
                <ul>
                    <?php
                    // foreach( $data as $clubinformation ) {
                    foreach ($data as $allclubs) {
                        $clubName = $allclubs->name;
                        $clubKurzel = $allclubs->code;
                        $president = $allclubs->president;
                        $street = $allclubs->street;
                        $postcode = $allclubs->postcode;
                        $city = $allclubs->city;
                        $phone = $allclubs->phone;
                        $website = $allclubs->website;

                        if ($clubName != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Boxclubname', 'swissboxing'); ?>:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $clubName; ?></div>
                            </li>
                            <?php
                        }
                        if ($clubKurzel != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Clubkürzel', 'swissboxing'); ?>:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $clubKurzel; ?></div>
                            </li>
                            <?php
                        }
                        if ($president != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Präsident', 'swissboxing'); ?>/in:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $president; ?></div>
                            </li>
                            <?php
                        }
                        if ($street != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Adresse', 'swissboxing'); ?>:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $street; ?><?php echo $postcode; ?><?php echo $city; ?></div>
                            </li>
                            <?php
                        }
                        if ($phone != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Telefon', 'swissboxing'); ?>:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $phone; ?></div>
                            </li>
                            <?php
                        }
                        if ($website != "") {
                            ?>
                            <li>
                                <div class="cabc-btext col-md-6"><b><?php _e('Website', 'swissboxing'); ?>:</b></div>
                                <div class="cabc-ltext col-md-6"><?php echo $website; ?></div>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-6">
                <?php
                foreach ($data as $imageLink) {
                    $imageLogo = $imageLink->logo;
                    ?>
                    <img src="<?php echo $imageLogo; ?>" alt="">
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="lizenzierte-athleten margin-bottom-3rem">
            <div class="title_article clearfix">
                <p class="title_article_text clearfix"><?php _e('Lizenzierte Athleten', 'swissboxing'); ?> (<?php _e('Total', 'swissboxing'); ?>: <?php echo $countathletes; ?>)</p>
            </div>
            <table class="table">
                <tbody>
                <tr>
                    <th><?php _e('Vorname', 'swissboxing'); ?></th>
                    <th><?php _e('Nachname', 'swissboxing'); ?></th>
                    <th><?php _e('Jahrgang', 'swissboxing'); ?></th>
                    <th><?php _e('LizenzNr', 'swissboxing'); ?></th>
                    <th><?php _e('Lizenz', 'swissboxing'); ?></th>
                    <th><?php _e('gültig bis', 'swissboxing'); ?> </th>
                    <th></th>
                </tr>
                <?php
                foreach ($data->rows->activeAthletes as $athletesinformation) {
                    $athleteinfoID = $athletesinformation->id;
                    $requestAthlete = wp_remote_get($api . "athletes/" . $athleteinfoID);
                    // echo "http://api.swissboxing.ch/WEB-AQMkADAwATY3/athletes/".$athleteinfoID;

                    if (is_wp_error($requestAthlete)) {
                        return false;
                    }
                    $bodyAthlete = wp_remote_retrieve_body($requestAthlete);
                    $dataAthlete = json_decode($bodyAthlete);

                    foreach ($dataAthlete as $athletes) {
                        if (empty($athletes->surname)) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td> <?php echo $athletes->surname; ?> </td>
                            <td> <?php echo $athletes->lastname; ?> </td>
                            <td> <?php echo $athletes->yearOfBirth; ?> </td>
                            <td> <?php echo $athletes->id; ?> </td>
                            <td> <?php
                                if ($athletes->licenceAOB->status = "Athlete-Licence") {
                                    echo "Athlete-Licence";
                                } else {
                                    echo "Athlete LC";
                                }
                                ?> </td>
                            <td> <?php echo $athletes->licenceAOB->validUntil; ?> </td>
                            <td><a href="boxmeetings-resultate/?athlete=<?php echo $athletes->id ?>"><?php _e('mehr', 'swissboxing'); ?></a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="lizenzierte-trainer margin-bottom-3rem">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Lizenzierte Trainer', 'swissboxing'); ?></p>
            </div>
            <table class="table">
                <tbody>
                <tr>
                    <th><?php _e('Vorname', 'swissboxing'); ?></th>
                    <th><?php _e('Nachname', 'swissboxing'); ?></th>
                    <th><?php _e('Lizenz', 'swissboxing'); ?></th>
                    <th><?php _e('gültig bis', 'swissboxing'); ?> </th>
                </tr>
                <?php
                foreach ($data->rows->activeCoaches as $trainerinformation) {
                    $trainersurname = $trainerinformation->surname;
                    $trainerlastname = $trainerinformation->lastname;
                    ?>
                    <tr>
                        <td><?php echo $trainersurname; ?></td>
                        <td><?php echo $trainerlastname; ?></td>
                        <td><a href="lizenzierte-trainer"> <?php _e('uncalled', 'swissboxing'); ?></a></td>
                        <td> <?php _e('uncalled', 'swissboxing'); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="organisierte-meetings margin-bottom-5rem">
            <div class="title_article clearfix">
                <p class="title_article_text clearfix"><?php _e('Organisierte Meetings', 'swissboxing'); ?></p>
            </div>
            <table class="table">
                <tbody>
                <tr>
                    <th><i><?php _e('Datum', 'swissboxing'); ?></i></th>
                    <th></th>
                    <th><i><?php _e('Meeting', 'swissboxing'); ?></i></th>
                    <th></th>
                </tr>
                <?php
                foreach ($data->rows->meetings as $meetingsinformation) {
                    $idmeeting = $meetingsinformation->id;
                    $websitemeeting = $meetingsinformation->_self;
                    ?>
                    <tr>
                        <td><?php echo $meetingsinformation->endDate ?></td>
                        <td><img src="<?php bloginfo('template_url'); ?>/img/approved.png" alt="Bewilligt"></td>
                        <td><?php echo $meetingsinformation->name ?></td>
                        <td class="right-align"><a href="boxmeeting-content?id=<?php echo $idmeeting ?>"><?php _e('mehr', 'swissboxing'); ?></a></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
