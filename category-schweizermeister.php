<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<?php
$request = wp_remote_get($api . "champions");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 " id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php the_title(); ?></p>
    </div>
    <div class="newsContent">
        <table class="table">
            <tbody>
            <thead>
            <th><?php _e('Gewicht', 'swissboxing'); ?></th>
            <th><?php _e('Meister/in', 'swissboxing'); ?></th>
            <th><?php _e('Club', 'swissboxing'); ?></th>
            <th></th>
            </thead>
            <tbody>
            </tbody>
            </tbody>
        </table>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
