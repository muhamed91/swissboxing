<?php
$protocol = is_SSL() ? 'https://' : 'http://';
$token = get_option('apiLink');
$api = $protocol . $token;
global $api;

register_nav_menus(array(
    'header_menu' => 'This is Header menu',
    'footer_menu' => 'This is Footer Menu',
));

function add_theme_scripts()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    // css styles
    wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/js/jquery-ui.min.css');

    // Js Scripts
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery.min.js');
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js');
    wp_enqueue_script('swissboxing', get_template_directory_uri() . '/js/swissboxing.js');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');
add_theme_support('post-thumbnails');

require_once get_template_directory() . '/includes/wpdocs-walker-nav-menu.php';


add_action('admin_init', 'my_general_section');
function my_general_section()
{
    add_settings_section(
        'my_settings_section', // Section ID
        '', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'apiLink', // Option ID
        'Api Link', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'apiLink' // Should match Option ID
        )
    );

    register_setting('general', 'apiLink', 'esc_attr');
}

function my_section_options_callback()
{ // Section Callback
    echo '<p>Here you should add the API LINK (EX. api.howeverlink.com/token/)</p>';
}

function my_textbox_callback($args)
{  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="' . $args[0] . '" name="' . $args[0] . '" value="' . $option . '" />';
}

add_action( 'after_setup_theme', 'my_theme_setup' );
  function my_theme_setup(){
  load_theme_textdomain( 'swissboxing, get_template_directory() . '/languages );
}

?>
