<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<?php
$request = wp_remote_get($api . "athletes");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
foreach ($data->rows as $athleteinfo) {
    $idja = $athleteinfo->id;
    $requestemri = wp_remote_get($api . "athletes/" . $idja);
    if (is_wp_error($requestemri)) {
        return false;
    }
    $body1 = wp_remote_retrieve_body($requestemri);
    $data1 = json_decode($body1);
    foreach ($data1 as $athleteinfos) {
        $id = $athleteinfos->id;
        $surename = $athleteinfos->surname;
        $lastname = $athleteinfos->lastname;
        $yearOfBirth = $athleteinfos->yearOfBirth;
        $clubOfBox = $athleteinfos->club->name;
        $weight = $athleteinfos->weight;
        $gender = $athleteinfos->gender;
        $palmaresAOBWin = $athleteinfos->palmaresAOB->win;
        $palmaresAODraw = $athleteinfos->palmaresAOB->equal;
        $palmaresAOLose = $athleteinfos->palmaresAOB->loose;
        // echo $surename;
        $needle = $_GET['searchboxer'];
        if (strpos($surename, $needle) !== false) {
            $GLOBALS['resultMatched'] = "
													<tr>
														<td>" . $surename . "</td>
														<td>" . $lastname . "</td>
														<td>" . $id . "</td>
														<td>" . $yearOfBirth . "</td>
														<td>" . $clubOfBox . "</td>
														<td>" . $palmaresAOBWin . '(+)' . $palmaresAOLose . ' (-) ' . $palmaresAODraw . " </td>
													</tr>
																";
        }
    }
}
?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <div class="col-lg-12">
            <p class="att-cbs-info"><b><?php _e('Hier können Sie nach einem Boxer suchen. Der Suchbegriff muss aus mindestens 3
                    Zeichen bestehen. Gesucht wird über den Vornamen, Nachnamen, Jahrgang und Boxclub. Es kann auch nur
                    ein Teil des Namens eingegeben werden.', 'swissboxing'); ?></b></p>
        </div>
        <div class="col-lg-12">
            <div class="boxer-suchen-form">
                <form>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <label><?php _e('Suchbegriff.', 'swissboxing'); ?></label>
                            </td>
                            <td>
                                <input name="searchboxer" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input name="submit" type="submit" id="submit" value="Suchen">
                            </td>
                        </tr>
                        <tr>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <th><?php _e('Vorname', 'swissboxing'); ?></th>
                    <th><?php _e('Nachname', 'swissboxing'); ?></th>
                    <th><?php _e('Lizenz', 'swissboxing'); ?></th>
                    <th><?php _e('Jahrgang', 'swissboxing'); ?></th>
                    <th><?php _e('Boxclub', 'swissboxing'); ?></th>
                    <th><?php _e('Palmarès', 'swissboxing'); ?></th>
                    </thead>
                    <tbody id="search_gagner_content">
                    <?php
                    echo $GLOBALS['resultMatched'];
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
