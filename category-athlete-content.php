<?php
$athleteId = $_GET['athlete'];
$request = wp_remote_get($api . "athletes/" . $athleteId);
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
?>
<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php _e('Robert Nicolet Trophy', 'swissboxing'); ?></h3>
    </div>
    <?php
    foreach ($data as $athleteinfo) {
        $athleteSurename = $athleteinfo->surname;
        $athleteLastnamee = $athleteinfo->lastname;
    }
    ?>
    <div class="newsContent">
        <p><?php _e('Die Punktewertung für', 'swissboxing'); ?> <?php echo $athleteSurename . ' ' . $athleteLastnamee; ?> <?php _e('setzt sich wie folgt', 'swissboxing'); ?>
            <?php _e('zusammen', 'swissboxing'); ?>:</p>
        <div class="title_article">
            <p class="title_article_text"><?php _e('Robert Nicolet Trophy 2013, Detailwertung', 'swissboxing'); ?>
                <?php _e('von', 'swissboxing'); ?> <?php echo $athleteSurename . ' ' . $athleteLastnamee; ?></p>
        </div>
        <table class="table">
            <tbody>
            <tr>
                <th><?php _e('Datum', 'swissboxing'); ?></th>
                <th><?php _e('Veranstaltung', 'swissboxing'); ?></th>
                <th><?php _e('Resultat', 'swissboxing'); ?></th>
                <th><?php _e('Bewertungsregel', 'swissboxing'); ?></th>
                <th><?php _e('Punkte', 'swissboxing'); ?></th>
            </tr>
            <?php
            foreach ($data->rows->fightsAOB as $athleteinfo) {
                $meetingDate = $athleteinfo->date;
                $meetingName = $athleteinfo->meeting->name;
                $meetingStatus = $athleteinfo->status;
                ?>
                <tr>
                    <td><?php echo $meetingDate; ?></td>
                    <td><?php echo $meetingName; ?></td>
                    <td><?php echo $meetingStatus ?></td>
                    <td><?php _e('Wettkampf im Ausland', 'swissboxing'); ?></td>
                    <td>+15</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
