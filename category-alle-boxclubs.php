<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<?php
$request = wp_remote_get($api . "clubs");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
function numberToRomanRepresentation($number)
{
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if ($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}
?>
<br>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <!--   <div class="headerNews text-right">
      <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
      </div> -->
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#overiviewAllboxer"><?php _e('Overview', 'swissboxing'); ?></a></li>
    </ul>
    <div class="tab-content">
        <div id="overiviewAllboxer" class="tab-pane fade in active">
            <table class="table">
                <thead>
                <tr>
                    <th><?php _e('Ort', 'swissboxing'); ?></th>
                    <th><?php _e('Boxclub', 'swissboxing'); ?></th>
                    <th><?php _e('Region', 'swissboxing'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($data->rows as $product) {
                    $prdoc = $product->id;
                    $request = wp_remote_get($protocol . "api.swissboxing.ch/WEB-AQMkADAwATY3/clubs/" . $prdoc);
                    if (is_wp_error($request)) {
                        return false;
                    }
                    $bodyid = wp_remote_retrieve_body($request);
                    $dataid = json_decode($bodyid);
                    foreach ($dataid as $productid) {
                        $clubID = $productid->id;
                        $regionName = $productid->region;
                        $clubName = $productid->name;
                        $cityName = $productid->city;
                        $websiteLink = $productid->website;
                        if ($clubName != "") {
                            ?>
                            <tr>
                                <td class="clubsNames"><?php echo $cityName ?></td>
                                <td><?php echo $clubName ?></td>
                                <td>
                                    <?php echo numberToRomanRepresentation($regionName) ?>
                                </td>
                                <?php
                                // echo $websiteLink;
                                if ($websiteLink != "") {
                                    ?>
                                    <td>
                                        <a href="<?php echo $websiteLink; ?>" target="_blank">
                                            <img src="<?php bloginfo('template_url'); ?>/img//Terrain.png"
                                                 width="20" height="20">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="alle-boxclubs-content?club=<?php echo $clubID; ?>"><?php _e('mehr', 'swissboxing'); ?></a>
                                    </td>
                                    <?php
                                } else {
                                    echo "";
                                }
                                ?>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
