<?php get_header(); ?>
<?php
$data = file_get_contents($api . "meetings");
$todayDate = date("d.m.Y");
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <?php
        $json = json_decode($data, true);
        foreach ($json as $key => $value) {
            // $endDate = $val['startDate'];
            if (!is_array($value)) {
                // echo $key . '=>' . $value . '<br/>';
            } else {
                foreach ($value as $key => $val) {
                    $endDate = $val['startDate'];
                    if (strtotime($endDate) < strtotime('-1 day')) {
                        ?>
                        <li class="clearfix">
                            <div class="col-lg-4 date"><?php echo $val['startDate'] ?></div>
                            <div class="col-lg-4"><?php echo $val['name'] ?></div>
                            <!-- <?php echo $val['_self'] ?> -->
                            <div class="col-lg-4 text-right"><a href="boxmeeting-content?id=<?php echo $val['id'] ?>"><?php _e('Mehr', 'swissboxing'); ?></a>
                            </div>
                        </li>
                        <?php
                    }
                }
            }
        }
        ?>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
