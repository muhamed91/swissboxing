<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php _e('Links international', 'swissboxing'); ?></p>
    </div>
    <div class="newsContent">
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                $links_type = get_field("links_type");

                if ($links_type === 'Links international') {
                    the_content();
                }
            endwhile;
        else :
            echo wpautop(__( 'Keine Dokumente vorhanden', 'swissboxing' ));
        endif;
        ?>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php _e('Links national', 'swissboxing'); ?></p>
    </div>
    <div class="newsContent">
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                $links_type = get_field("links_type");

                if ($links_type === 'Links national') {
                    the_content();
                }
            endwhile;
        else :
            echo wpautop(__( 'Keine Dokumente vorhanden', 'swissboxing' ));
        endif;
        ?>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
