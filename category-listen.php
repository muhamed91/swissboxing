<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <div class="title_article">
            <p class="title_article_text"><?php _e('Trainer und Präsidentenlisten', 'swissboxing'); ?></p>
        </div>
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                ?>
                <div>
                    <p>
                        <small><?php the_content() ?></small>
                    </p>
                </div>
            <?php
            endwhile;
        else :
            echo wpautop(__( 'Keine Dokumente vorhanden', 'swissboxing' ));
        endif;
        ?>
        <div class="col-lg-12">
            <p class="att-listen">
                <b><?php _e('Hinweis: Diese Listen werden automatisch auf Basis der uns bekannten Daten generiert. Sie haben also
                    immer die aktuellsten Listen zum Download zur Verfügung. Bei Fehlern werden Sie sich bitte an die
                    Geschäftsstelle.', 'swissboxing'); ?></b>
            </p>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
