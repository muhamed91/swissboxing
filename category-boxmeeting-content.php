<?php get_header(); ?>
<?php
$meetingid = $_GET['id'];
$request = wp_remote_get($api . "meetings/" . $meetingid);
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php single_cat_title(); ?></p>
    </div>
    <div class="newsContent">
        <div class="eboxing-information">

            <table width="100%" border="0">
                <tbody>
                <tr>
                    <td><b><?php _e('Veranstaltung', 'swissboxing'); ?>:</b></td>
                    <td>
                        <?php
                        foreach ($data as $meetings) {
                            echo $meetings->name;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><b><?php _e('Datum', 'swissboxing'); ?>:</b></td>
                    <td><?php _e('Freitag', 'swissboxing'); ?>,
                        <?php
                        foreach ($data as $product) {
                            echo $product->startDate;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><b><?php _e('Bewilligung', 'swissboxing'); ?>:</b></td>
                    <td><img src="<?php bloginfo('template_url'); ?>/img/approved.png" alt="Bewilligt"><?php _e('Meeting wurde bewilligt', 'swissboxing'); ?>
                    </td>
                </tr>
                <tr>
                    <td><b><?php _e('Veranstalter', 'swissboxing'); ?>:</b></td>
                    <td>
                        <?php
                        // Get the ID of a given category
                        $category_id = get_cat_ID('Alle Boxclubs Content');
                        // Get the URL of this category
                        $category_all_boxclubs_content_link = get_category_link($category_id);
                        // echo $category_link;
                        foreach ($data->rows as $product) {
                            ?>
                            <a href="<?php $category_all_boxclubs_content_link ?>?id=<?php echo $product->id; ?>">
                                <?php
                                echo $product->name;
                                ?>
                            </a>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><b><?php _e('Veranstaltungsort', 'swissboxing'); ?>:</b></td>
                    <td>
                        <?php
                        foreach ($data as $product) {
                            echo $product->address;
                        }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="dokumente-kontent">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Dokumente', 'swissboxing'); ?></p>
            </div>
            <div class="dokumente">

                <?php
                foreach ($data->rows->documents as $product) {
                    if (!empty($product->name)) {
                        $fileType = $product->type;
                        ?>
                        <img src="<?php bloginfo('template_url'); ?>/img/<?php echo $fileType ?>.png"> <a
                                href="<?php echo $product->_href ?>"><?php echo $product->name; ?></a>
                        <?php
                    } else {
                        echo _e( 'Not document', 'swissboxing' );
                    }
                }
                ?>
            </div>
        </div>
        <div class="Amateurkampfe-kontent">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Amateurkämpfe (Hinweis: Profikämpfe werden hier nicht aufgeführt)', 'swissboxing'); ?></p>
            </div>
            <div class="Amateurkampfe">
                <table class="table">
                    <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                        <th><?php _e('Rote Ecke', 'swissboxing'); ?> </th>
                        <th><?php _e('Club', 'swissboxing'); ?></th>
                        <th><?php _e('Blaue Ecke', 'swissboxing'); ?> </th>
                        <th><?php _e('Club', 'swissboxing'); ?></th>
                        <th><?php _e('Gewicht', 'swissboxing'); ?></th>
                        <th class="text-center" colspan="2"><?php _e('Resultat', 'swissboxing'); ?></th>
                    </tr>
                    <?php
                    foreach ($data->rows->fights as $product) {
                        $productHref = $product->redCorner->_self;
                        echo $productid;
                        $requestAthlete = wp_remote_get($productHref);
                        if (is_wp_error($requestAthlete)) {
                            return false;
                        }
                        $bodyAthlete = wp_remote_retrieve_body($requestAthlete);
                        $dataAthlete = json_decode($bodyAthlete);

                        // echo $product->redCorner->surname;
                        $winnerColor = $product->winner;
                        ?>
                        <tr>
                            <td><?php echo $product->fightNr; ?></td>
                            <?php
                            if ($winnerColor === 'BLUE') {
                                // echo "blue";
                                ?>
                                <td class="fights-result-color frc-blue"></td>
                                <?php
                            } elseif ($winnerColor === 'DRAW') {
                                // echo "draw";
                                ?>
                                <td class="fights-result-color frc-draw"></td>
                                <?php
                            } elseif ($winnerColor === 'RED') {
                                // echo "red";
                                ?>
                                <td class="fights-result-color frc-red"></td>
                                <?php
                            } else {
                                // echo "something else";
                            }
                            ?>
                            <td>
                                <?php
                                $bluelink = $product->blueCorner->id;
                                $redlink = $product->redCorner->id;
                                // Get the ID of a given category
                                $category_bxomeetings_resultate = get_cat_ID('Boxmeetings / Resultate');
                                // Get the URL of this category
                                $category_bxomeetings_resultate_link = get_category_link($category_bxomeetings_resultate);
                                ?>
                                <a href="<?php if ($redlink != null) {
                                    echo $category_bxomeetings_resultate_link . '?athlete=' . $redlink;
                                } else {
                                    echo "#";
                                } ?>">
                                    <?php echo $product->redCorner->lastname; ?>
                                    <br>
                                    <i><?php echo $product->redCorner->surname; ?></i>
                                </a>
                            </td>
                            <td><?php echo $product->redCorner->club->code; ?></td>
                            <td>
                                <a href="<?php if ($bluelink != null) {
                                    echo $category_bxomeetings_resultate_link . '?athlete=' . $bluelink;
                                } else {
                                    echo "#";
                                } ?>">
                                    <?php echo $product->blueCorner->lastname; ?>
                                    <br>
                                    <i><?php echo $product->blueCorner->surname; ?></i>
                                </a>
                            </td>
                            <td><?php echo $product->blueCorner->club->code; ?></td>
                            <?php

                            foreach ($dataAthlete as $productAthlete) {
                                $weightClass = $productAthlete->weightClass;

                            }
                            ?>
                            <td><?php
                                if (!empty($weightClass)) {
                                    echo $weightClass;
                                } else {
                                    echo "-";
                                }
                                ?></td>
                            <td><?php echo $product->result; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <p class="resutls-explain">
                    <span class="re-red"><?php _e('Rote Ecke gewinnt', 'swissboxing'); ?></span>
                    <span class="re-blue"><?php _e('Blaue Ecke gewinnt', 'swissboxing'); ?></span>
                    <span class="re-gray-light"><?php _e('Unentschieden', 'swissboxing'); ?></span>
                    <span class="re-black"><?php _e('Kampf ohne Wertung', 'swissboxing'); ?></span>
                    <span class="re-gray-dark"><?php _e('beide verloren', 'swissboxing'); ?></span>
                </p>
            </div>
        </div>
        <div class="offiziele">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Offizielle', 'swissboxing'); ?></p>
            </div>
            <table class="col-lg-12" border="0">
                <tbody>
                <?php
                foreach ($data->rows->officials as $product) {
                    ?>
                    <tr>
                        <td class="rolename">
                            <b><?php echo $product->roleName; ?></b>
                            <br>
                        </td>
                        <td class="rolename-surlastname text-left">
                            <?php echo $product->offical->surname; ?> <?php echo $product->offical->lastname; ?><br>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
