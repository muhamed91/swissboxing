<div class="col-lg-2 col-md-3 col-sm-3 padding-right-0" id="aktuelleNews_desktop">
    <div class="headerAktuelleNews">
        <span class="hal-item"><?php _e('Aktuelle News', 'swissboxing'); ?></span>
    </div>
    <div class="aktuelleNews-List">
        <ul>
            <?php
            // the query
            $the_query = new WP_Query(array(
                'category_name' => 'News',
                'post_status' => 'publish',
                'posts_per_page' => 6,
            ));
            // Get the ID of a given category
            $category_id = get_cat_ID('News');
            // Get the URL of this category
            $category_link = get_category_link($category_id);
            ?>
            <?php if ($the_query->have_posts()) : ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="cl-item">
                        <li><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></li>
                    </div>
                    <!-- <div class="cl-item-divider"></div> -->
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php __('No News'); ?></p>
            <?php endif; ?>
        </ul>
    </div>
    <div class="posters">
        <?php
        // the query
        $the_query = new WP_Query(array(
            'category_name' => 'Events',
            'post_status' => 'publish',
            'posts_per_page' => 5,
        ));
        ?>
        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="posters-item">
                    <?php the_post_thumbnail(); ?>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
    </div>
</div>
