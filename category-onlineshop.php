<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php single_cat_title(); ?></p>
    </div>
    <div class="newsContent">
        <?php
        $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
        if (have_posts()) :
            while (have_posts()) : the_post();
                ?>
                <div class="col-lg-12 product-row">
                    <div class="col-lg-2 image-product">
                        <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                    </div>
                    <div class="col-lg-10">
                        <b><?php the_title() ?></b>
                        <p><?php the_content() ?></p>
                        <p><?php _e( 'Stückpreis', 'swissboxing' ); ?>: <b>CHF <?php the_field('product_price') ?>.</b>- (<?php _e('einschliesslich Porto', 'swissboxing'); ?>):</p>
                        <p><?php _e('Bestellung bei', 'swissboxing'); ?>: <a href="mailto:<?php the_field('contact_email') ?>" target="_top"><?php the_field('contact_email') ?></a></p>
                    </div>
                </div>
            <?php
            endwhile;
        else :
            echo wpautop(__( 'Sorry, no posts were found', 'swissboxing' ));
        endif;
        ?>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
