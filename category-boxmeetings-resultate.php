<?php get_header(); ?>
<?php
$AthleteId = $_GET['athlete'];
$request = wp_remote_get($api . "athletes/" . $AthleteId);
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php _e('Boxmeetings / Resultate', 'swissboxing'); ?></h3>
    </div>
    <div class="newsContent">
        <div class="col-lg-12">
            <?php
            foreach ($data as $athleteinfo) {
                // echo  $athleteinfo->surname;
                $id = $athleteinfo->id;
                $surname = $athleteinfo->surname;
                $lastname = $athleteinfo->lastname;
                $yearOfBirth = $athleteinfo->yearOfBirth;
                $clubname = $athleteinfo->club->name;
                $licenceStatus = $athleteinfo->licenceAOB->status;
                $licenceValidDate = $athleteinfo->licenceAOB->validUntil;
                $weight = $athleteinfo->weight;
                $weightClass = $athleteinfo->weightClass;
                $gender = $athleteinfo->gender;
            }
            ?>
            <table class="col-lg-12" border="0">
                <tbody>
                <tr>
                    <td class="col-lg-4"><b><?php _e('LizenzNr', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $id; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Vorname', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $surname; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Nachname', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $lastname; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Jahrgang', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $yearOfBirth; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Geschlecht', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $gender; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Kampfgewicht', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $weight . 'kg'; ?> (<?php echo $weightClass; ?>)</td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Boxclub', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $clubname; ?></td>
                </tr>
                <tr>
                    <td class="col-lg-4"><b><?php _e('Lizenzen', 'swissboxing'); ?>:</b></td>
                    <!-- <td><img src="data/modul_webdesign/true.png" alt="" style="height:12px;width:12px;"> 31.12.2018<br></td> -->
                    <td>
                        <img src="<?php echo bloginfo('template_directory') ?>/img/true.png" alt="">
                        <?php _e('Athlete-Licence', 'swissboxing'); ?> - <?php _e('Lizenz gültig bis', 'swissboxing'); ?> <?php echo $licenceValidDate; ?>
                        <br></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <img src="<?php echo bloginfo('template_directory') ?>/img/tab_sb_active.png">
            <div class="divider-cbresultate"></div>
        </div>
        <div class="col-lg-12">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Palmarès von', 'swissboxing'); ?> <?php echo $surname . " " . $lastname; ?></p>
            </div>
            <table class="table table-borderless">
                <tbody>
                <?php foreach ($data as $athletefights) {
                } ?>
                <tr>
                    <td><b><?php _e('Total Kämpfe', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $athletefights->palmaresAOB->fights; ?></td>
                </tr>
                <tr>
                    <td><b><?php _e('davon gewonnen', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $athletefights->palmaresAOB->win; ?></td>
                </tr>
                <tr>
                    <td><b><?php _e('davon verloren', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $athletefights->palmaresAOB->loose; ?></td>
                </tr>
                <tr>
                    <td><b><?php _e('davon unentschieden', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $athletefights->palmaresAOB->nocount; ?></td>
                </tr>
                <tr>
                    <td><b><?php _e('davon ohne Wertung', 'swissboxing'); ?>:</b></td>
                    <td><?php echo $athletefights->palmaresAOB->nocount; ?></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Robert Nicolet Trophy', 'swissboxing'); ?> </p>
            </div>
            <?php
            $request = wp_remote_get($api . "trophy/aob/2018");
            if (is_wp_error($request)) {
                return false;
            }
            $body2018 = wp_remote_retrieve_body($request);
            $data2018 = json_decode($body2018);
            foreach ($data2018->rows as $rnt) {
                $idAthlete2018 = $rnt->athlete->id;
                if ($idAthlete2018 == $id) {
                    $rank = $rnt->rank;
                    $points = $rnt->points;
                }
            }
            ?>
            <table class="table">
                <tbody>
                <tr>
                    <th><?php _e('Jahr', 'swissboxing'); ?></th>
                    <th><?php _e('Rang', 'swissboxing'); ?></th>
                    <th><?php _e('Punkte', 'swissboxing'); ?></th>
                    <th></th>
                </tr>
                <tr>
                    <td><a href="robert-nicolet-trophy"><?php _e('Robert Nicolet Trophy 2018', 'swissboxing'); ?></a></td>
                    <td><?php echo $rank; ?></td>
                    <td><?php echo $points; ?></td>
                    <td><a href="athlete-content/?athlete=<?php echo $id ?>"><?php _e('mehr...', 'swissboxing'); ?></a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Kämpfe von', 'swissboxing'); ?> <?php echo $surname . " " . $lastname; ?></p>
            </div>
            <i><?php _e('Anmerkung: Die Liste kann aufgrund von ausländischen Kämpfen oder fehlenden Nachtragungen nicht
                abschliessend sein.', 'swissboxing'); ?></i>
            <br>
            <b><?php _e('Keine Kämpfe.', 'swissboxing'); ?></b>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
