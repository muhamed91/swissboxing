<?php get_header(); ?>
<div class="top_slider">
    <img src="<?php bloginfo('template_url'); ?>/img/boxmen.jpg" class="ts_item">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 padding-all-0">
            <!-- left -->
            <?php get_sidebar('left'); ?>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
                <div class="headerNews text-right">
                    <h3 class="hn-title"><?php _e('News', 'swissboxing'); ?></h3>
                </div>
                <?php
                // the query
                $the_query = new WP_Query(array(
                    'category_name' => 'News',
                    'post_status' => 'publish',
                    'posts_per_page' => 5,
                ));
                ?>
                <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="article_preview_homepage clearfix">
                            <div class="col-lg-12 padding-all-0">
                                <div class="col-lg-3 padding-all-0">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
                                </div>
                                <div class="col-lg-9 padding-all-0">
                                    <div class="col-lg-12">
                                        <p><b><?php echo the_title(); ?></b></p>
                                    </div>
                                    <div class="col-lg-12">
                                        <p><?php
                                            $link = get_the_permalink();
                                            $content = get_the_content();
                                            echo mb_strimwidth($content, 0, 380, " "); ?>
                                            <a href="<?php echo $link ?>" class="article_read_more"><?php _e('Read More', 'swissboxing'); ?></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else : ?>
                    <p><?php __('No News'); ?></p>
                <?php endif; ?>
            </div>
            <!-- right -->
            <?php get_sidebar('right'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
