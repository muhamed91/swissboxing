<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php single_cat_title(); ?></p>
    </div>
    <div class="newsContent">
        <table class="table">
            <thead>
            <tr>
                <th><?php _e('Datum', 'swissboxing'); ?></th>
                <th><?php _e('Beschreibung', 'swissboxing'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    $dateFrom = get_field("date_from");
                    $dateTo = get_field("date_to");
                    ?>
                    <tr>
                        <td><?php echo $dateFrom . '<br>' . $dateTo; ?></td>
                        <td><?php the_title(); ?></td>
                        <td><a href="<?php the_permalink(); ?>"><?php _e('mehr', 'swissboxing'); ?></a></td>
                    </tr>
                <?php
                endwhile;
            else :
                echo wpautop(__( 'Sorry, no posts were found', 'swissboxing' ));
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
