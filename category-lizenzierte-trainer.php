<?php get_header(); ?>
<?php
$request = wp_remote_get($api . "coaches");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <table class="table">
            <thead>
            <tr>
                <th><?php _e('Vorname', 'swissboxing'); ?></th>
                <th><?php _e('Nachname', 'swissboxing'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data->rows as $athleteinfo) {
                $athleteSurename = $athleteinfo->surname;
                $athleteLastnamee = $athleteinfo->lastname;
                ?>
                <tr>
                    <td><?php echo $athleteSurename; ?></td>
                    <td><?php echo $athleteLastnamee; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
