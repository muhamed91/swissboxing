</div>
<footer>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-content">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 nw-social"></div>
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 copyright">
            <p class="text-center">© <?php echo date("Y"); ?> - swissboxing.ch</p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 exLinks">
            <ul>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer_menu')
                );
                ?>
            </ul>
        </div>
    </div>
</footer>
</body>
</html>
