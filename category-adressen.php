<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <td>
            <div id="tab_content" class="tab_content">
                <div id="tabstitle" class="title_article">
                    <p class="title_article_text"><?php single_cat_title(); ?></p>
                </div>
                <div id="1_tab tabcontenti">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            $type_of_selection = get_field("adressen_type_of_selection");
                            $photo = get_field("adressen_photo");
                            $name = get_field("adressen_name");
                            $role = get_field("adressen_role");
                            $adresa = get_field("adressen_adresa");
                            ?>
                            <table class="table" data-selection="<?php echo $type_of_selection; ?>">
                                <tbody>
                                <tr>
                                    <?php
                                    $image = get_field('adressen_photo');
                                    if (!empty($image)):
                                        // vars
                                        $alt = $image['alt'];
                                        // thumbnail
                                        $size = 'thumbnail';
                                        $thumb = $image['sizes'][$size];
                                        $width = $image['sizes'][$size . '-width'];
                                        $height = $image['sizes'][$size . '-height'];
                                        ?>
                                        <td>
                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>"/>
                                        </td>
                                    <?php endif; ?>
                                    <td valign="middle">
                                        <b><?php echo $name; ?></b><br><i><?php echo $role; ?></i><br><br><?php echo $adresa; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $phone_G = get_field("phone_G");
                                        $phone_M = get_field("phone_M");
                                        $phone_F = get_field("phone_F");
                                        if (!empty($phone_G)) {
                                            ?><p><?php echo $phone_G; ?></p><?php
                                        } else {
                                            ?><p><?php  _e('Nummer G ist nicht deklariert', 'swissboxing'); ?></p><?php
                                        }
                                        if (!empty($phone_M)) {
                                            ?><p><?php echo $phone_M; ?></p><?php
                                        } else {
                                            ?><p><?php  _e('Nummer M ist nicht deklariert', 'swissboxing'); ?></p><?php
                                        }
                                        if (!empty($phone_F)) {
                                            ?><p><?php echo $phone_F; ?></p><?php
                                        } else {
                                            ?><p><?php _e('Nummer F ist nicht deklariert', 'swissboxing'); ?></p><?php
                                        }
                                        ?>
                                    </td>
                                    <td valign="middle" class="typeofselection"><?php
                                        echo the_field("adressen_type_of_selection");
                                        ?></td>
                                </tr>
                                </tbody>
                            </table>
                        <?php
                        endwhile;
                    else :
                        echo wpautop(__( 'Keine Dokumente vorhanden', 'swissboxing' ));
                    endif;
                    ?>
                </div>
            </div>
        </td>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
