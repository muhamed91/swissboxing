<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<?php
$data = file_get_contents($api . "meetings");
$json = json_decode($data, true);
$todayDate = date("d.m.Y");
?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#meetingscarriedout" aria-controls="meetingscarriedout" role="tab" data-toggle="tab"><?php _e('durchgeführte Meetings.', 'swissboxing'); ?></a></li>
            <li role="presentation"><a href="#office" aria-controls="profile" role="tab" data-toggle="tab"><?php _e('geplante Meetings.', 'swissboxing'); ?></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active boxmeetings-list" id="meetingscarriedout">
                <ul id="mainList">
                    <?php
                    foreach ($json as $key => $value) {
                        if (!is_array($value)) {
                            // echo $key . '=>' . $value . '<br/>';
                        } else {
                            foreach ($value as $key => $val) {
                                $endDate = $val['startDate'];
                                if (strtotime($endDate) < strtotime('-1 day')) {
                                    ?>
                                    <li class="clearfix">
                                        <div class="col-lg-4 date"><?php echo $val['startDate'] ?></div>
                                        <div class="col-lg-4"><?php echo $val['name'] ?></div>
                                        <div class="col-lg-4 text-right"><a
                                                    href="boxmeeting-content?id=<?php echo $val['id'] ?>"><?php _e('Mehr.', 'swissboxing'); ?></a></div>
                                    </li>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
            <div role="tabpanel" class="tab-pane " id="office">
                <ul id="mainList">
                    <?php
                    foreach ($json as $key => $value) {
                        if (!is_array($value)) {
                        } else {
                            foreach ($value as $key => $val) {
                                $endDate = $val['startDate'];
                                if (strtotime($endDate) > strtotime('-1 day')) {
                                    ?>
                                    <li class="clearfix">
                                        <div class="col-lg-4 date"><?php echo $val['startDate'] ?></div>
                                        <div class="col-lg-4"><?php echo $val['name'] ?></div>
                                        <div class="col-lg-4 text-right"><a href="boxmeeting-content?id=<?php echo $val['id'] ?>"><?php _e('Mehr.', 'swissboxing'); ?></a></div>
                                    </li>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
