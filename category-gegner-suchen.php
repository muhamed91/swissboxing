<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<?php
$request = wp_remote_get($api . "athletes");
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);
foreach ($data->rows as $athleteinfo) {
    $idja = $athleteinfo->id;
    $requestemri = wp_remote_get($api . "athletes/" . $idja);
    if (is_wp_error($requestemri)) {
        return false;
    }
    $body1 = wp_remote_retrieve_body($requestemri);
    $data1 = json_decode($body1);
    foreach ($data1 as $athleteinfos) {
        $surename = $athleteinfos->surname;
        $lastname = $athleteinfos->lastname;
        $yearOfBirth = $athleteinfos->yearOfBirth;
        $clubOfBox = $athleteinfos->club->name;
        $weight = $athleteinfos->weight;
        $gender = $athleteinfos->gender;
        $palmaresAOBWin = $athleteinfos->palmaresAOB->win;
        $palmaresAODraw = $athleteinfos->palmaresAOB->equal;
        $palmaresAOLose = $athleteinfos->palmaresAOB->loose;
        // echo $yearOfBirth;
        ?>
        <p><?php
            if (!empty($yearOfBirth)) {
                // code...

                $yob = array($yearOfBirth);
                $wig = array($weight);
                $gen = array($gender);
                if (in_array($_GET['Year'], $yob) && in_array($_GET['Weight'], $wig) && in_array($_GET['Gender'], $gen)) {
                    $GLOBALS['resultMatched'] = "
															<tr>
																<td>" . $surename . "</td>
																<td>" . $lastname . "</td>
																<td>" . $yearOfBirth . "</td>
																<td>" . $clubOfBox . "</td>
																<td>" . $palmaresAOBWin . '(+)' . $palmaresAOLose . ' (-) ' . $palmaresAODraw . " </td>
															</tr>";
                } else {
                    // echo "dosent exist";
                }
            }
            ?></p>
        <?php
    }
}
?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <div class="col-lg-12">
            <p class="att-cgs-info"><b><?php _e('Hier können Sie einen geeigneten in der Schweiz lizenzierten Gegner für Ihren
                    Boxer suchen. Geben Sie dazu den Jahrgang, Kampfgewicht und Geschlecht des Boxers an. Es werden nur
                    Boxer mit einer gültigen Lizenz angezeigt', 'swissboxing'); ?>.</b></p>
            <div class="form-cgs">
                <form>
                    <input name="action" type="hidden" value="searchCombatant">
                    <table width="99%" border="0">
                        <tbody>
                        <tr>
                            <td><label><?php _e('Jahrgang', 'swissboxing'); ?></label></td>
                            <td><input name="Year" id="s_year" type="text" size="10" maxlength="4"></td>
                        </tr>
                        <tr>
                            <td><label><?php _e('Kampfgewicht', 'swissboxing'); ?></label></td>
                            <td><input name="Weight" id="s_weight" type="text" size="10" maxlength="6"> kg</td>
                        </tr>
                        <tr>
                            <td><label><?php _e('Geschlecht', 'swissboxing'); ?></label></td>
                            <td>
                                <input type="radio" value="MALE" name="Gender" checked="checked"><?php _e('männlich', 'swissboxing'); ?> <br>
                                <input type="radio" value="FEMALE" name="Gender"><?php _e('weiblich', 'swissboxing'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input name="submit" type="submit" id="gagnersucheqn" value="<?php _e('Gegner suchen', 'swissboxing'); ?>"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <table class="table">
                <thead>
                <th><?php _e('Vorname', 'swissboxing'); ?></th>
                <th><?php _e('Nachname', 'swissboxing'); ?></th>
                <th><?php _e('Jahrgang', 'swissboxing'); ?></th>
                <th><?php _e('Boxclub', 'swissboxing'); ?></th>
                <th><?php _e('Palmarès', 'swissboxing'); ?></th>
                </thead>
                <tbody id="search_gagner_content">
                <?php
                echo $GLOBALS['resultMatched'];
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
