<?php get_header(); ?>
<?php get_sidebar('left'); ?>

<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 " id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="title_article">
        <p class="title_article_text"><?php the_title(); ?></p>
    </div>
    <div class="newsContent">
        <table class="table tvCat">
            <tbody>
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    ?>
                    <tr>

                        <td><?php the_content() ?></td>
                    </tr>
                <?php
                endwhile;
            else :
                echo wpautop(__( 'no content any detected', 'swissboxing' ));
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
