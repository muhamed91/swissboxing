<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <table class="table">
            <tbody>
            <tr>
                <th><?php _e('Quiz', 'swissboxing'); ?></th>
                <th><?php _e('Beschreibung', 'swissboxing'); ?></th>
                <th></th>
            </tr>
            <tr>
                <td><b><?php _e('Fortbildung Leistungstest - Wettkampftrainer AOB', 'swissboxing'); ?></b></td>
                <td><?php _e('Leistungstest fuer SwissBoxing Wettkampftrainer im Olympischen Boxen', 'swissboxing'); ?></td>
                <td><span><?php _e('bitte anmelden', 'swissboxing'); ?></span>
                    <img src="<?php bloginfo('template_url'); ?>/img/lock.png" alt="<?php _e('Nur für registrierte Mitglieder', 'swissboxing'); ?>">
                </td>
            </tr>
            <tr>
                <td><b><?php _e('Fortbildung Trainer Light-Contact Boxing', 'swissboxing'); ?></b></td>
                <td><?php _e('Offizielles Fortbildungsquiz für Jugend- und Light-Contact Trainer', 'swissboxing'); ?></td>
                <td><span><?php _e('bitte anmelden', 'swissboxing'); ?></span>
                    <img src="<?php bloginfo('template_url'); ?>/img/lock.png" alt="Nur für registrierte Mitglieder">
                </td>
            </tr>
            </tbody>
        </table>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Login', 'swissboxing'); ?></p>
            </div>
            <p><?php _e('Um die offiziellen Quizzes zu absolvieren, müssen Sie sich anmelden.', 'swissboxing'); ?></p>
            <div id="login_data" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <small><?php _e('Die Logininformationen finden Sie auf Ihrer gültigen SwissBoxing-Lizenz', 'swissboxing'); ?></small>
                    <form action="#" method="post">
                        <div class="form-group">
                            <label for="email"><?php _e('LizenzID (1)', 'swissboxing'); ?> </label>
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password"><?php _e('SB-Code (2)', 'swissboxing'); ?> </label>
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <button type="button" name="button" class="btn btn-default"><?php _e('Login', 'swissboxing'); ?></button>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="<?php bloginfo('template_url'); ?>/img/liz_card.png" alt="Lizenz">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="title_article">
                <p class="title_article_text"><?php _e('Verantwortliche', 'swissboxing'); ?></p>
            </div>
            <p><?php _e('Du hast eine veraltete Frage/Antwort entdeckt oder hast eine gute Idee für eine zusätzliche Frage? Wende
            dich mit einem möglichst konkreten Verbesserungsvorschlag an die verantwortliche Person.
            Verantwortlich für Fortbildung Trainer Light-Contact + Fitness Boxing: Stefan Käser
            Verantwortlich für Fortbildung Trainer AOB (Olympisches Boxen): Michael Sommer', 'swissboxing'); ?></p>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
