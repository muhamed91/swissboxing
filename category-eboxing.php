<?php get_header(); ?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="list-group clearfix">
        <div class="col-lg-6">
            <a href="boxmeetings"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/boxmeetings.png"></a>
        </div>
        <div class="col-lg-6">
            <a href="alle-boxclubs"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/boxclubs.png"></a>
        </div>
        <div class="col-lg-6">
            <a href="gegner-suchen"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gegner_suchen.png"></a>
        </div>
        <div class="col-lg-6">
            <a href="boxer-suchen"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/boxer_suchen.png"></a>
        </div>
        <div class="col-lg-6">
            <a href="listen"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/listen.png"></a>
        </div>
        <div class="col-lg-6">
            <a href="robert-nicolet-trophy"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rn_ranking.png"></a>
        </div>
    </div>
    <div class="newsContent">
        <table class="table tvCat">
            <tbody>
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    ?>
                    <div class="eboxing_articles">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </div>
                <?php
                endwhile;
            else :
                echo wpautop(__( 'Sorry, no posts were found', 'swissboxing' ));
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
