<!--
    ________  ____________________  __   _____ ____  __    __  ________________  _   _______
   /  _/ __ \/_  __/ ____/ ____/ / / /  / ___// __ \/ /   / / / /_  __/  _/ __ \/ | / / ___/
   / // /_/ / / / / __/ / /   / /_/ /   \__ \/ / / / /   / / / / / /  / // / / /  |/ /\__ \
 _/ // _, _/ / / / /___/ /___/ __  /   ___/ / /_/ / /___/ /_/ / / / _/ // /_/ / /|  /___/ /
/___/_/ |_| /_/ /_____/\____/_/ /_/   /____/\____/_____/\____/ /_/ /___/\____/_/ |_//____/
--> <!--
    Theme & Plugins developed by IRTECH GmbH
    More Informations @ https://www.ir-tech.ch
-->
<!DOCTYPE html>
<html lang="<?php bloginfo('language') ?>">
<head>
    <!-- <meta charset="UTF-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo get_bloginfo( 'name' ); ?> - <?php echo get_bloginfo( 'description' ); ?></title>

    <!-- Bootstrap -->
    <!--     <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
     -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body>

<header>
    <nav class="navbar navbar-swissboxing" id="Desktop_Navbar">
        <div class="container-fluid">
            <div class="menu-content">
                <div class="logo-header text-center">
                    <a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png"></a>
                </div>
                <div class="menu-list text-center ">
                    <div class="menu-links">
                        <?php
                        wp_nav_menu(array(
                            'menu' => 'header menu',
                            'walker' => new WPDocs_Walker_Nav_Menu()
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>
    <nav class="navbar navbar-swissboxing" id="Mobile_Navbar">
        <div class="container-fluid">
            <div class="menu-content-mobile">
                <div id="logo-header-mobile" class="logo-header-mobile text-center col-xs-4">
                    <a href="/"> <img src="<?php bloginfo('template_url'); ?>/img/logo.png" class="img-responsive"></a>
                </div>
                <div class="navTitle col-xs-4 text-center">
                    <h3><?php  _e('News', 'swissboxing'); ?></h3>
                </div>
                <div id="hamburger-icon" class="hamburger-icon col-xs-4 text-right">
                    <div id="nav-icon1">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <div id="swissboxingToggle" class="swissboxingLeftmenu">
            <ul>
                <!-- <li><a href="#">dasds</a></li> -->
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'header_menu',
                        'menu_id' => 'main_mobile_ul'
                    )
                );
                ?>
            </ul>
        </div>
    </nav>
</header>
<div class="content">
