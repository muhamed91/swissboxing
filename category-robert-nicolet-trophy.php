<?php get_header(); ?>
<?php
// DATA FOR 2018
$request = wp_remote_get($api . "trophy/aob/2018");
if (is_wp_error($request)) {
    return false;
}
$body2018 = wp_remote_retrieve_body($request);
$data2018 = json_decode($body2018);
// DATA FOR 2017
$request2017 = wp_remote_get($api . "trophy/aob/2017");
if (is_wp_error($request2017)) {
    return false;
}
$body2017 = wp_remote_retrieve_body($request2017);
$data2017 = json_decode($body2017);
// DATA FOR 2016
$request2016 = wp_remote_get($api . "trophy/aob/2016");
if (is_wp_error($request2016)) {
    return false;
}
$body2016 = wp_remote_retrieve_body($request2016);
$data2016 = json_decode($body2016);
// DATA FOR 2015
$request2015 = wp_remote_get($api . "trophy/aob/2015");
if (is_wp_error($request2015)) {
    return false;
}
$body2015 = wp_remote_retrieve_body($request2015);
$data2015 = json_decode($body2015);
// DATA FOR 2014
$request2014 = wp_remote_get($api . "trophy/aob/2014");
if (is_wp_error($request2014)) {
    return false;
}
$body2014 = wp_remote_retrieve_body($request2014);
$data2014 = json_decode($body2014);
// DATA FOR 2013
$request2013 = wp_remote_get($api . "trophy/aob/2013");
if (is_wp_error($request2013)) {
    return false;
}
$body2013 = wp_remote_retrieve_body($request2013);
$data2013 = json_decode($body2013);
?>
<?php get_sidebar('left'); ?>
<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="newsContent_desktop">
    <div class="headerNews text-right">
        <h3 class="hn-title cat_name"><?php single_cat_title(); ?></h3>
    </div>
    <div class="newsContent">
        <div class="col-lg-12 text-right">
            <p class="att-crnt"><i><?php _e('Punktestand hat sich kürzlich geändert Updated', 'swissboxing'); ?> <img  src="<?php bloginfo('template_url'); ?>/img/updated.png"></i></p>
        </div>
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab2018">2018</a></li>
                <li class=""><a data-toggle="tab" href="#tab2017">2017</a></li>
                <li class=""><a data-toggle="tab" href="#tab2016">2016</a></li>
                <li class=""><a data-toggle="tab" href="#tab2015">2015</a></li>
                <li class=""><a data-toggle="tab" href="#tab2014">2014</a></li>
                <li class=""><a data-toggle="tab" href="#tab2013">2013</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab2018" class="tab-pane fade in active">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2018->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="<?php _e('männlich', 'swissboxing'); ?>"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div id="tab2017" class="tab-pane fade">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2017->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="männlich"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div id="tab2016" class="tab-pane fade">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2016->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="männlich"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div id="tab2015" class="tab-pane fade">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2015->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="<?php _e('männlich', 'swissboxing'); ?>"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div id="tab2014" class="tab-pane fade">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2014->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="männlich"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div id="tab2013" class="tab-pane fade">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th></th>
                            <th><?php _e('Rang', 'swissboxing'); ?></th>
                            <th></th>
                            <th><?php _e('Vorname', 'swissboxing'); ?></th>
                            <th><?php _e('Nachname', 'swissboxing'); ?></th>
                            <th><?php _e('Club', 'swissboxing'); ?></th>
                            <th><?php _e('Punkte', 'swissboxing'); ?></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($data2013->rows as $rnt) {
                            // echo  $rnt->rank . "<br>";
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $rnt->rank; ?></td>
                                <td><img src="<?php bloginfo('template_url'); ?>/img/male.png" alt="männlich"></td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->surname; ?></a>
                                </td>
                                <td>
                                    <a href="boxmeetings-resultate?athlete=<?php echo $rnt->athlete->id; ?>"><?php echo $rnt->athlete->lastname; ?></a>
                                </td>
                                <td><?php echo $rnt->athlete->club->name; ?></td>
                                <td class="right-align"><?php echo $rnt->points; ?></td>
                                <td class="right-align"><a  href="athlete-content?athlete=<?php echo $rnt->athlete->id; ?>"><?php _e('mehr...', 'swissboxing'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12 list-information-zur-rangierung">
            <div class="title_article">
                <p class="title_article_text"><?php _e('*Information zur Rangierung', 'swissboxing'); ?></p>
            </div>
            <div class="col-lg-12">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        ?>
                        <p><?php the_content() ?></p>
                    <?php
                    endwhile;
                else :
                    echo wpautop(__( 'Sorry, no posts were found', 'swissboxing' ));
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
