<div class="col-lg-2 col-md-3 col-sm-3" id="links_desktop">
    <div class="headerLinks">
        <span class="hl-item">Links</span>
    </div>
    <div class="contentLinks">
        <?php
        // the query
        $the_query = new WP_Query(array(
            'category_name' => 'Sponsors',
            'post_status' => 'publish',
            'posts_per_page' => 5,
        ));
        ?>
        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="cl-item">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="cl-item-divider"></div>

            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>

        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
    </div>
</div>
